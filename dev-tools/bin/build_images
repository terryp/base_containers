#! /bin/bash
# NOTE: This is run in busybox ash when run in the CI/CD environment

set -e

if [[ -z "$SRC" ]]; then
    echo "SRC must be set and point to the code tree" 1>&2
    exit 1
fi

REGISTRY=registry.gitlab.com/terryp/base_containers
if [[ -n "$CI_JOB_ID" && "$CI_COMMIT_BRANCH" = "$CI_DEFAULT_BRANCH" ]]; then
    IS_MAIN_BUILD=true
    echo "Building for release"
else
    IS_MAIN_BUILD=false
    echo "Testing build process locally"
fi

inc_version() {
    old_ver="$1"

    # pull out the last part while saving the prefix
    so_far=''
    sep=''
    last=''
    for part in $(echo "$old_ver" | tr '.' ' '); do
        so_far="${so_far}${last}${sep}"
        sep='.'
        last="$part"
    done

    # just add one
    let new="$last"+1

    echo "${so_far}${new}"
}


rebuild_container() {
    name="$1"
    old_ver="$2"
    new_ver="$3"

    # Encourage re-use by pulling the old containers, this will cause the new
    # builds to re-use the existing parts.
    for vrs in ${old_ver} ${new_ver}; do
        docker pull "$REGISTRY/${name}:${vrs}" >/dev/null 2>&1 || true
    done

    cd "$SRC/deploy/${name}"
    docker build --rm -t "$REGISTRY/${name}:${new_ver}" .

    if "$IS_MAIN_BUILD"; then
        docker push "$REGISTRY/${name}:${new_ver}"
    else
        echo "Skipping docker push for test"
    fi
}


replace_ver() {
    name="$1"
    new_sum="$2"
    new_ver="$3"

    # fixup any references to the old version in Dockerfiles
    sed -i \
        "s@${REGISTRY}/${name}:[0-9.-]*\$@${REGISTRY}/${name}:${new_ver}@" \
        $SRC/deploy/*/Dockerfile

    # fixup the version file for next time
    sed -i \
        "s@^[0-9a-zA-Z]* ${name} [0-9.-]*\$@${new_sum} ${name} ${new_ver}@" \
        "$SRC/deploy/docker.vers"
}


check_change() {
    name="$1"
    old_sum="$2"
    old_ver="$3"

    new_sum=$(cd "$SRC/deploy"; \
        find "${name}" -name \*.swp -prune -o -type f -print0 \
            | LC_ALL=C sort -z -f \
            | xargs -0 sha1sum \
            | sha1sum \
            | cut -d ' ' -f1)

    if [[ "$old_sum" != "$new_sum" ]]; then
        new_ver=$(inc_version "$old_ver")

        echo
        echo
        echo
        echo "Container $name has changed, building $new_ver"

        rebuild_container "$name" "$old_ver" "$new_ver"

        replace_ver "$name" "$new_sum" "$new_ver"
    elif ! docker pull "$REGISTRY/${name}:${old_ver}" 2>/dev/null; then
        echo
        echo
        echo
        echo "Container $name version $old_ver not found on registry, building"

        rebuild_container "$name" "$old_ver" "$old_ver"
    fi
}


# Check each container for changes
grep -vE '^#|^$' "$SRC/deploy/docker.vers" | while read sum name ver
do
    check_change "$name" "$sum" "$ver"
done

if ! git diff --quiet --exit-code; then
    if $IS_MAIN_BUILD; then
        # In a CI/CD build, we changed something, check it in
        git commit -m "Auto build containers" \
            deploy/docker.vers \
            deploy/*/Dockerfile

        AUTH="gitlab_ci_runner:$WRITE_TOKEN"
        HOSTPATH="${CI_REPOSITORY_URL#*@}"

        git push "https://$AUTH@${HOSTPATH}" HEAD:$CI_COMMIT_BRANCH
    else
        echo "Skipping commit and git push for test"
    fi
fi
